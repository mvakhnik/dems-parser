//dems js, needs jQuery

$(document).ready(


    function () {
        var Model = {
            Url: "Url",
            updateAt: "updatedAt",
            View: "View"
        };
        var Const = {
            durShow: 600,
            durHide: 200,
            durError: 400,

            clrError: 'rgb(248, 189, 189)'
        };
        var Text = {
            logout: 'Logout',
            login: 'Login'
        };
        var htmlElements = {
            btnLogin: $('#btnLogin'),
            btnRegister: $('#btnRegister'),
            loginInpLogin: $('#loginInpLogin'),
            loginInpPass: $('#loginInpPass'),
            loginBtnCancel: $('#loginBtnCancel'),
            bntLoginLogout: $('#btnLoginLogout'),
            panelLogin: $('#pnlLogin'),
            msgLoginError: $('#msgLoginError'),

            panelRegister: $('#pnlRegister'),
            regInpLogin: $('#regInpLogin'),
            regInpPass: $('#regInpPass'),
            regInpPassConf: $('#regInpPassConf'),
            regBtnRegister: $('#regBtnRegister'),
            regBtnCancel: $('#regBtnCancel')

        };
        //init parse
        Parse.initialize('JCGvHhBOncwWqUGJJwVMSj11X8WnbNMFZhdLDmLZ', '9LY8O3MNAP5n0uzTXWoQxB6D78mivV67UNSgkFGz');



        function getCurrentUsername() {
            var currUser = Parse.User.current();
            if (currUser) {
                return Parse.User.current().get("username");
            }
            return "";
        }

        function changeGuiToCurrentUser(username) {
            htmlElements.bntLoginLogout.text(Text.logout);
            $('#userName').text('Hello, ' + username);
        }

        function changeGuiToAnonym() {
            console.log('changeGuiToAnonym');
            htmlElements.bntLoginLogout.text(Text.login);
            $('#userName').text('Dems project with parser.com');
        }

        function refreshSession() {
            var currentUserName = getCurrentUsername();

            console.log('refreshSession', currentUserName);
            if (currentUserName) {
                changeGuiToCurrentUser(currentUserName);

            } else {
                changeGuiToAnonym();
            }
        }
        /**
         * Login and init refresh session, gui...
         * @param result - callback object {success : function (user),
         *                  error : function(user, error)}
         */
        function login(name, password, result) {
            if (name && password) {
                console.log("login:", name, password);
                Parse.User.logIn(name, password, {
                    success: function (user) {

                        if (result) {
                            result.success(user);
                        }

                    },
                    error: function (user, error) {

                        if (result && result.error) {
                            result.error(user, error);
                        }

                    }
                });
            }
        }

        /**
         * Logouts and init refresh session, gui...
         */
        function logout() {
            Parse.User.logOut();

        }

        function getLogin() {
            var login = htmlElements.loginInpLogin.val();

            return login;
        }

        function getPassword() {
            return htmlElements.loginInpPass.val();
        }


        function processLoginLogout(value) {
            console.log("processLoginLogout", value);
            if (value === Text.login) {
                showLoginPanel();
                //                login();

            } else {
                logout();
                refreshSession();
            }
        }

        function showLoginPanel() {
            console.log("showLoginPanel");
            htmlElements.panelLogin.slideDown();
        }

        function hideLoginPanel() {
            htmlElements.panelLogin.slideUp();
        }

        function showErrorOnElement(jqElement) {
            var bc = jqElement.css("background-color");
            jqElement.animate({
                backgroundColor: Const.clrError
            }, Const.durError, function () {

                jqElement.css("background-color", bc);
            });
        }

        function showError(parseError) {
            $(".alert").alert();
            console.log("showError:", parseError);
            showErrorOnElement(htmlElements.loginInpLogin);
            showErrorOnElement(htmlElements.loginInpPass);

        }




        function showRegPanel() {
            htmlElements.panelRegister.slideDown();
        }

        function hideRegPanel() {
            htmlElements.panelRegister.slideUp();
        }

        function checkName(name) {
            console.log("check name:", name);
            if (name) {
                return true;
            }
            return false;
        }

        function checkPass(pass) {
            if (pass && pass.length > 5) {
                return true;
            }
            return false;
        }

        function startRegistration() {
            var name = htmlElements.regInpLogin.val();
            var pass = htmlElements.regInpPass.val();
            var passConf = htmlElements.regInpPassConf.val();
            console.log("name:", name);
            if (!checkName(name)) {
                showErrorOnElement(htmlElements.regInpLogin);
                return;
            }

            if (!checkPass(pass)) {
                showErrorOnElement(htmlElements.regInpPass);
                return;
            }
            if (pass !== passConf) {
                showErrorOnElement(htmlElements.regInpPass);
                showErrorOnElement(htmlElements.regInpPassConf);
                return;
            }

            register(name, pass, {
                success: function (user) {
                    console.log("successfully register:", user);
                    hideRegPanel();
                    refreshSession();
                },
                error: function (user, error) {

                    if (error.code == 202) {
                        console.log("error registration:", error.message);
                        showErrorOnElement(htmlElements.regInpLogin);
                    }
                }
            });
        }

        function register(name, pass, callback) {
            var user = new Parse.User();
            user.set("username", name);
            user.set("password", pass);


            user.signUp(null, {
                success: function (user) {

                    if (callback && callback.success) {
                        user.setACL(new Parse.ACL(user));
                        user.save(null, {
                            error: function (userAgain, error) {
                                console.log("error save ACL:", error);

                            }
                        });
                        callback.success(user);
                    }

                },
                error: function (user, error) {

                    if (callback && callback.error) {
                        callback.error(user, error);
                    }

                }
            });
        }

        /* Fetchs defined number of viewed media
             for certain user. If no viewed - returns last count
             of urls.
             Returns Url's array
         */
        function getLastViewedMedia(username, count) {
            var Url = Parse.Object.extend(Model.Url);
            var query = new Parse.Query(Url);
            var now = new Date().toISOString();
            console.log("Url: " + Url);
            console.log("now: " + now);
            query.lessThanOrEqualTo(Model.updateAt, now);

            query.find({
                success: function (results) {

                },
                error: function (error) {
                    console.log("Error: " + error.code + " " + error.message);
                }
            });
        }


        function initMedia() {
            //            var result = query.lessThanOrEqualTo(Model.updateAt, now);

            getLastViewedMedia(getCurrentUsername(), Const.nextCount, {
                success: function (results) {
                    console.log("Successfully retrieved " + results.length + " Urls.");
                    // Do something with the returned Parse.Object values
                    for (var i = 0; i < results.length; i++) {
                        var object = results[i];
                        console.log(object);
                    }
                }
            });
            //            if (nextMedia) {
            //                var currentMedia = getCurrentMedia(nextMedia);
            //                var previousMedia = getPreviousMedia(currentMedia);
            //
            //                setCurrent(currentMedia);
            //                addNextMedia(nextMedia);
            //                addPreviousMedia(previousMedia);
            //            }






        }

        function temp() {
            $('#btnTest').click(function (event) {
                initMedia();
            });
        }

        function bind() {

            htmlElements.btnLogin.click(function (event) {

                var name = getLogin();
                var pass = getPassword();

                login(name, pass, {
                    success: function (user) {

                        console.log("success:", user);
                        refreshSession();
                        hideLoginPanel();
                    },
                    error: function (user, error) {
                        console.log("error login:", user, error);
                        showError(user, error);
                    }
                });
            });
            htmlElements.loginBtnCancel.click(function (event) {
                hideLoginPanel();
            });
            htmlElements.bntLoginLogout.click(function (event) {
                processLoginLogout(htmlElements.bntLoginLogout.text());
            });

            htmlElements.btnRegister.click(function (event) {
                showRegPanel();
            });
            htmlElements.regBtnRegister.click(function (event) {
                startRegistration();
            });
            htmlElements.regBtnCancel.click(function (event) {
                hideRegPanel();
            });

            temp();
        }

        refreshSession();
        bind();

    }


)
